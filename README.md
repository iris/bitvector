# bitvector : MOVED TO stdpp-unstable

**THIS LIBRARY HAS BEEN MOVED TO THE stdpp-unstable LIBRARY. THIS REPOSITORY ONLY EXISTS FOR BACKWARDS COMPATIBILITY.**

# Old Readme

This project contains an implementation of bitvectors (fixed size integers) in Coq based on std++.

This project provides the `bv n` type for bitvectors with `n` bits and a range of functions to manipulate this
type. See [theories/bitvector.v](theories/bitvector.v) for details.

## Prerequisites

This version is known to compile with:

 - Coq version 8.11.2 / 8.12.2 / 8.13.2 / 8.14.1

## Installing via opam

To obtain a development version, add the Iris opam repository:

    opam repo add iris-dev https://gitlab.mpi-sws.org/iris/opam.git

Then you can do `opam install coq-bitvector`.

## Building from source

Run `make -jN` in this directory to build the library, where `N` is the number
of your CPU cores.  Then run `make install` to install the library.

## Contributing to bitvector

If you want to report a bug, please use the
[issue tracker](https://gitlab.mpi-sws.org/iris/bitvector/issues).  You will have to
create an account at the
[MPI-SWS GitLab](https://gitlab.mpi-sws.org/users/sign_in) (use the "Register"
tab).

To contribute code, please send your MPI-SWS GitLab username to
[Ralf Jung](https://gitlab.mpi-sws.org/jung) to enable personal projects for
your account.  Then you can fork the
[bitvector git repository](https://gitlab.mpi-sws.org/iris/bitvector), make your
changes in your fork, and create a merge request.

## Common problems

On Windows, differences in line endings may cause tests to fail. This can be 
fixed by setting Git's autocrlf option to true:

    git config --global core.autocrlf true
